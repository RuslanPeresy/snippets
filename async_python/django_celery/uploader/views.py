from django.shortcuts import render
from django.http import HttpResponse
from .tasks import upload

# Create your views here.


def async_job(request):
    local_path = '/home/ruslan/repos/snippets/async_python/django_celery/uploader/sample.mp4'
    path = 'celery-videos/sample.mp4'

    upload.apply_async(args=[local_path, path], ignore_result=True)

    return HttpResponse('Start uploading...')
