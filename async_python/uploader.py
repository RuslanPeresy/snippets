from tasks import upload

local_path = 'sample.MP4'
path = 'celery-videos/sample.MP4'

upload.apply_async(args=[local_path, path], ignore_result=True)

print('Start uploading...')