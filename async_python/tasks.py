from celery import Celery
from s3client import upload_video

app = Celery('tasks', broker='amqp://async_python:12345@localhost:5672')


@app.task
def add(x, y):
    return x + y


@app.task(name='cloudstorage.upload')
def upload(local_path, path):
    upload_video(local_path, path)
    return 'success'
