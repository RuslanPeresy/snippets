const authNav = [
  {
    path: '/login',
    name: 'login',
    component: () => import('../views/Auth/Login/index.vue'),
  },
  {
    path: '/register',
    name: 'register',
    component: () => import('../views/Auth/Register/index.vue'),
  }
];

export { authNav };