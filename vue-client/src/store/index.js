import Vue from 'vue';
import Vuex from 'vuex';
import video from './video';
import auth from './auth';

Vue.use(Vuex);

export default new Vuex.Store({
  state: {
  },
  mutations: {
  },
  actions: {
  },
  modules: {
    video,
    auth,
  }
})
