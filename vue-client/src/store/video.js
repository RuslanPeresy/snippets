import $http from '@/api';

const SET_ALL_VIDEOS = 'SET_ALL_VIDEOS';

export default {
  namespaced: true,
  state: {
    videos: [],
  },
  mutations: {
    [SET_ALL_VIDEOS](state, payload) {
      state.videos = payload;
    }
  },
  actions: {
    async getAllVideos({ commit }) {
      try {
        const { data } = await $http.get('/main');

        commit(SET_ALL_VIDEOS, data);

        return { data, error: null };
      } catch (error) {
        const {
          data: { message },
          status,
        } = error.response;

        return { data: null, error: { message, status } };
      }
    }
  },
  getters: {
    allVideos(state) {
      return state;
    }
  }
};