import os


class Config:
    SECRET_KEY = '025b376adf584b72888bffe69f90524d'
    CORS_ALLOWED_ORIGINS = ['http://localhost:8080']
    CELERY_BROKER_URL = os.environ.get('MESSAGE_QUEUE')
    SOCKETIO_MESSAGE_QUEUE = os.environ.get('MESSAGE_QUEUE')
